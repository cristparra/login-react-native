/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TextInput,
  KeyboardAvoidingView,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';


export default class Login extends React.Component {
  constructor(props){
    super(props);
    this.state={
      username:'',
      password:'',
    }
  }
  /*componentDidMount(){
    this._loadInitialState().done();
  }*/
  
 /* _loadInitialState=async()=>{
    var value= await AsyncStorage.getItem('user');
    if(value !== null){
      this.props.navigation.navigate('Profile');
    }
  }*/

  render() {
    return (
        
        <View style={styles.container}>
          
        <ImageBackground
        style={styles.logo}
        source={require('./Logo-bHealth.jpg')}
        imageStyle={{ resizeMode: 'contain' }}
        />

        <Text style={styles.header}> - Personal Médico - </Text>

          <TextInput
          style={styles.textinput} placeholder='correo electrónico'
          onChangeText={(username)=>this.setState({username})}
          underlineColorAndroid='black'>
          </TextInput>
         
          <TextInput
          secureTextEntry={true}
          style={styles.textinput} placeholder='Contraseña'
          onChangeText={(password)=>this.setState({password})}
          underlineColorAndroid='black'>
          </TextInput>

          <TouchableOpacity
          style={styles.btn}
          onPress={this.login}>
            <Text style={{color:'white'}}>Ingresar</Text>
          </TouchableOpacity>
          
          <TouchableOpacity
          style={styles.btn2}
          onPress={this.register}>
            <Text style={{color:'black'}}>Registro</Text>
          </TouchableOpacity>
          
          <TouchableOpacity
          style={styles.btn2}
          onPress={this.forgotPassword}>
            <Text style={{color:'black'}}>Olvidé mi contraseña</Text>
          </TouchableOpacity>

        </View>
    );
  }

  login = ()=> {
    alert(this.state.username);
    /*fetch('http://192.168.1.55:3000/users',{
      method:'POST',
      headers:{
        'Acept':'application/json',
        'Content-Type':'application/json',
      },
      body:JSON.stringify({
        username: this.state.username,
        password : this.state.password,
      })
    })
    .then((response)=> response.json())
    .then((res) => {
      if(res.succes === true){
        AsyncStorage.setItem('user',res.user);
        this.props.navigation.navigate('Profile');
      }
      else{
        alert(res.message);
      }
    })
    .done();
  */
  }
  forgotPassword = ()=> {
    alert(this.state.password)
  }
  
  register=() =>{
    alert(this.state.username+" " + this.state.password)
    
  }

}



const styles = StyleSheet.create({
  wrapper:{
    flex:1,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    paddingLeft:40,
    paddingRight:40,
  },
  header: {
    fontSize: 24,
    textAlign: 'center',
    marginBottom: 60,
    color:'black',
    fontWeight:'bold',
  },
  textinput: {
    textAlign: 'center',
    alignSelf:'stretch',
    padding:16,
    marginBottom:20,
    backgroundColor: '#fff',

  },
  btn:{
    alignSelf:'stretch',
    backgroundColor:'#e00b0b',
    padding:20,
    alignItems:'center',
  },
  btn2:{
    alignSelf:'stretch',
    backgroundColor:'#ffffff',
    padding:20,
    alignItems:'center',
  },
  logo:{
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft:150,
    paddingRight:150,
  },
});
