/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';


export default class Profile extends React.Component {

  render() {
    return (
      <KeyboardAvoidingView behavior='padding' style={styles.wrapper}>
        <View style={styles.container}>
         
          <Text style={styles.header}> - Profile - </Text>
          

        </View>
      </KeyboardAvoidingView>
    );
  }

  }

const styles = StyleSheet.create({
  wrapper:{
    flex:1,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#8fbdd3',
    paddingLeft:40,
    paddingRight:40,
  },
  header: {
    fontSize: 24,
    textAlign: 'center',
    marginBottom: 60,
    color:'black',
    fontWeight:'bold',
  },
  textinput: {
    alignSelf:'stretch',
    padding:16,
    marginBottom:20,
    backgroundColor: '#fff',

  },
  btn:{
    alignSelf:'stretch',
    backgroundColor:'#01c853',
    padding:20,
    alignItems:'center',
  },
});
